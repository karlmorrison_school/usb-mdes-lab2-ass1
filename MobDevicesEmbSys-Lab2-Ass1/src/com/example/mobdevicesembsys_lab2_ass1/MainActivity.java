package com.example.mobdevicesembsys_lab2_ass1;

import android.os.Bundle;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.view.Menu;
import android.view.View;

public class MainActivity extends Activity{

	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		FragmentManager fm = this.getFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		View fc = findViewById(R.id.fragment_container);
		
		FragmentMiddle fragMiddle = new FragmentMiddle();

		ft.replace(fc.getId(), fragMiddle);
		ft.addToBackStack(null);
		ft.commit();
	}
	
}
