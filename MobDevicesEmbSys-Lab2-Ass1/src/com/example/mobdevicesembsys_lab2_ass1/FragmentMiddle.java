package com.example.mobdevicesembsys_lab2_ass1;

import java.lang.Thread.State;

import se.goransson.microbridge.android.AdbListener;
import se.goransson.microbridge.android.Microbridge;
import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.AvoidXfermode;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ToggleButton;

public class FragmentMiddle extends Fragment {

	private Microbridge usb;
	
	private ProgressBar progressBar;
	private TextView progressText;
	private int progressBarStatus = 0;
	private Thread progressThread;
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}
	
	@Override 
	public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState);
		
		progressThread = new Thread(new Runnable() {
			@Override
			public void run() {
				while(progressBarStatus<100) {
					progressBarStatus += 1;
					try {
						Thread.sleep(1000);
						getActivity().runOnUiThread(new Runnable() {
							@Override
							public void run() {
								testProgressBar();
							}

							private void testProgressBar() {
								progressBar.setProgress(progressBarStatus);
								progressText.setText(progressBarStatus+"/"+progressBar.getMax());
							}	
						});
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}		
		});
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_middle, container, false);
		
		progressBar = (ProgressBar) rootView.findViewById(R.id.progress_bar);		
		progressText = (TextView) rootView.findViewById(R.id.progress_text);
		
		return rootView;
	}
	
	@Override
	public void onStart() {
		super.onStart();
		if(progressThread.getState()==State.NEW)
			progressThread.start();
	}

	@Override 
	public void onResume() { 
		//usb.connect(); 
		super.onResume();
	}
	
	@Override 
	public void onPause() { 
		//usb.stop(); 
		super.onPause(); 
	}

	private class UsbListener implements AdbListener { 
		@Override public void adbConnected() { 
			usb = new Microbridge(new UsbListener());
		}

		@Override public void adbDisconnected() { 
			// TODO Auto-generated method stub 
		}

		@Override public void adbEvent(byte[] buffer) { 
			progressBar.setProgress(buffer[0]);
		} 
	}
	
	private class ToggleListener implements OnClickListener { 
		@Override public void onClick(View v) { 
			boolean on = ((ToggleButton) v).isChecked(); 
			if(on) 
				usb.write("H".getBytes());
			else 
				usb.write("L".getBytes()); 
		} 
	}

}